<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'first_name' => 'Gabriel',
            'last_name'=> 'Sales',
            'email' => 'gabriel@gabriel.com',
            'password'=>bcrypt('password1')
        ]);

        User::factory()
            ->count(5)
            ->create();

    }
}

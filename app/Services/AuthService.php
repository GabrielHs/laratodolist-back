<?php


namespace App\Services;


use App\Events\ForgotPassword;
use App\Events\UserRegistered;
use App\Exceptions\LoginInvalidException;
use App\Exceptions\ResetPasswordTokenInvakidException;
use App\Exceptions\UserHasBeenTakenException;
use App\Exceptions\VerifyEmailTokenException;
use App\Mail\WelcomeMail;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

/**
 * Class AuthService
 * @package App\Services
 */
class AuthService
{
    /**
     * @param string $email
     * @param string $pass
     * @return array
     * @throws LoginInvalidException
     */
    public function login(string $email, string $pass): array
    {
        $login = [
            'email' => $email,
            'password' => $pass
        ];

        if (!$token = auth()->attempt($login)) {
            throw new LoginInvalidException();
        }
        return [
            'acess_token' => $token,
            'token_type' => 'Bearer'
        ];

    }

    public function register(array $campos)
    {
        $user = User::where('email', $campos['email'])->exists();
        if (!empty($user)) {
            throw new UserHasBeenTakenException();
        }

        $userPassword = bcrypt($campos['password'] ?? Str::random(10));

        $user = User::create([
            'first_name' => $campos['first_name'],
            'last_name' => $campos['last_name'] ?? '',
            'email' => $campos['email'],
            'password' => $userPassword,
            'confirmation_token' => Str::random(60)
        ]);

        event(new UserRegistered(($user)));

        return $user;
    }

    public function verifyEmail(string $token)
    {
        $user = User::where('confirmation_token', $token)->first();

        if (empty($user)) {
            throw new VerifyEmailTokenException();
        }

        $user->confirmation_token = null;
        $user->email_verified_at = now();
        $user->save();

        return $user;

    }


    /**
     * @throws ResetPasswordTokenInvakidException
     */
    public function resetPassword(string $email, string $password, string $token)
    {
        $passReset = PasswordReset::where('email', $email)
            ->where('token', $token)
            ->first();

        if (empty($passReset)) {
            throw  new ResetPasswordTokenInvakidException();
        }

        $user = User::where('email', $email)->firstOrFail();

        $user->password= bcrypt($password);

        $user->save();

        PasswordReset::where('email', $email)->delete();

        return '';

    }

    /**
     * @param string $email
     * @return string
     */
    public function forgotPassword(string $email): string
    {
        $user = User::where('email', $email)->firstOrFail();

        $token = Str::random(60);

        PasswordReset::create([
            'email' => $user->email,
            'token' => $token
        ]);

        event(new ForgotPassword($user, $token));

        return '';

    }


}

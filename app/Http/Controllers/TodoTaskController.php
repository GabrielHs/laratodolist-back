<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoTaskUpdateRequest;
use App\Http\Resources\TodoTaskResource;
use App\Models\TodoTask;


class TodoTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * @param TodoTask $todoTask
     * @param TodoTaskUpdateRequest $request
     * @return TodoTaskResource
     */
    public function update(TodoTask $todoTask, TodoTaskUpdateRequest $request): TodoTaskResource
    {
        $this->authorize('update', $todoTask);

        $input = $request->validated();

        $todoTask->fill($input);

        $todoTask->save();

        return new TodoTaskResource($todoTask);

    }


    public function destroy(TodoTask $todoTask)
    {
        $this->authorize('destroy', $todoTask);

        $todoTask->delete();
    }







}

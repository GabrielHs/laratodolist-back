<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoStoreRequest;
use App\Http\Requests\TodoTaskRequest;
use App\Http\Requests\TodoUpdateRequest;
use App\Http\Resources\TodoResource;
use App\Http\Resources\TodoTaskResource;
use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class TodoController
 * @package App\Http\Controllers
 */
class TodoController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return TodoResource::collection(Auth::user()->todos);
    }

    /**
     * @param Todo $todo
     * @return TodoResource
     */
    public function show(Todo $todo): TodoResource
    {
        $this->authorize('view', $todo);
        $todo->load('tasks');
        return new TodoResource($todo);
    }

    /**
     * @param TodoStoreRequest $request
     * @return TodoResource
     */
    public function store(TodoStoreRequest $request): TodoResource
    {
        $input = $request->validated();

        $todo = Auth::user()->todos()->create($input);

        return new TodoResource($todo);
    }

    /**
     * @param Todo $todo
     * @param TodoUpdateRequest $request
     * @return TodoResource
     */
    public function update(Todo $todo, TodoUpdateRequest $request): TodoResource
    {
        $this->authorize('update', $todo);

        $input = $request->validated();

        $todo->fill($input);

        $todo->save();

        return new TodoResource($todo->fresh());
    }

    /**
     * @param Todo $todo
     * @return bool
     */
    public function destroy(Todo $todo): bool
    {
        $this->authorize('destroy', $todo);


        $todo->delete();

        return true;

    }

    /**
     * @param Todo $todo
     * @param TodoTaskRequest $request
     * @return TodoTaskResource
     */
    public function addTask(Todo $todo, TodoTaskRequest $request): TodoTaskResource
    {
        $this->authorize('addTask', $todo);

        $input = $request->validated();
        $todoTask = $todo->tasks()->create($input);

        return new TodoTaskResource($todoTask);

    }


}

<?php

namespace App\Http\Controllers;

use App\Exceptions\LoginInvalidException;
use App\Exceptions\UserHasBeenTakenException;
use App\Exceptions\VerifyEmailTokenException;
use App\Http\Requests\AuthForgotPasswordRequest;
use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthRegisterRequest;
use App\Http\Requests\AuthResetPasswordRequest;
use App\Http\Requests\AuthVerifyEmailRequest;
use App\Http\Resources\UserResource;
use App\Services\AuthService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    private $authService;

    /**
     * AuthController constructor.
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @param AuthLoginRequest $req
     * @return UserResource
     * @throws LoginInvalidException
     */
    public function login(AuthLoginRequest $req): UserResource
    {
        $input = $req->validated();


        $token =  $this->authService->login($input['email'] ,$input['password']);

        return (new UserResource(Auth::user()))->additional($token);

    }


    /**
     * @throws UserHasBeenTakenException
     */
    public function register(AuthRegisterRequest $req): UserResource
    {
        $input = $req->validated();
        $user = $this->authService->register($input);

        return new UserResource($user);
    }


    /**
     * @throws VerifyEmailTokenException
     */
    public function verifyEmail(AuthVerifyEmailRequest $req): UserResource
    {
        $input = $req->validated();

        $user =  $this->authService->verifyEmail($input['token']);

        return new UserResource($user);

    }


    /**
     * @param AuthForgotPasswordRequest $request
     * @return string
     */
    public function forgotPassword(AuthForgotPasswordRequest $request): string
    {
        $input = $request->validated();

        return $this->authService->forgotPassword($input['email']);

    }

    public function resetPassword(AuthResetPasswordRequest $request)
    {
        $input = $request->validated();

        return $this->authService->resetPassword($input['email'], $input['password'], $input['token']);

    }









}

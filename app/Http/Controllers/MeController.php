<?php

namespace App\Http\Controllers;

use App\Http\Requests\MeUpdateRequest;
use App\Http\Resources\UserResource;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MeController extends Controller
{
    /**
     * @return UserResource
     */
    public function index()
    {
        return new UserResource(Auth::user());
    }

    public function update(MeUpdateRequest $request)
    {

        $input = $request->validated();

        $user = (new UserService())->updateUser(Auth::user(), $input); // como é utilizado somente em um local não precisa injetar e parenteses é para poder os metodos da classe sem precisar colocar na variavel

        return new UserResource($user);




    }


}

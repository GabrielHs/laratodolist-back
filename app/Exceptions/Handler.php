<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {

        $this->renderable(function (Throwable $e, $req) {
          //dd($e);
            //if ($e instanceof ModelNotFoundException){
            // $modelName  = class_basename($e->getModel());

            $apiErroCode = "";
            $message = "";
            $code = 000;

            switch ($e->getMessage()) {

                case "Unauthenticated.":
                    $apiErroCode = "Unauthenticated";
                    $message = "Not Authentication";
                    $code= 401;
                    break;

                case "This action is unauthorized.":
                    $apiErroCode = "Unauthorized";
                    $message = "This action is unauthorized";
                    $code= 403;
                    break;

                default:
                    // var_dump(1);
                    $apiErroCode = "NotFoundException";
                    $message = " not  found";
                    $code= 404;
                    break;
            }

            return response()->json([
                'erro' => $apiErroCode,
                'message' => $message
            ], $code);
            //}
        });
    }
}

<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class ResetPasswordTokenInvakidException extends Exception
{
    protected $message = 'Reset password not valid.';

    public function render(): JsonResponse
    {

        return response()->json([
            'erro' => class_basename($this),
            'message' => $this->getMessage()
        ], 400);
    }
}

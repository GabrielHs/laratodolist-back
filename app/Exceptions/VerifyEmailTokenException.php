<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class VerifyEmailTokenException extends Exception
{
    protected $message = 'Token not valid.';

    public function render(): JsonResponse
    {

        return response()->json([
            'erro' => class_basename($this),
            'message' => $this->getMessage()
        ], 400);
    }
}

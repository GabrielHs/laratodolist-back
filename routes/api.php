<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MeController;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\TodoTaskController;
use Illuminate\Support\Facades\Route;


Route::prefix('v1')->group(function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('register', [AuthController::class, 'register'])->name('register');
    Route::get('verify-email', [AuthController::class, 'verifyEmail'])->name('verifyEmail');
    Route::post('forgot-password', [AuthController::class, 'forgotPassword'])->name('forgotPassword');
    Route::post('reset-password', [AuthController::class, 'resetPassword'])->name('resetPassword');

    Route::prefix('me')->middleware('auth:api')->group(function (){
        Route::get('', [MeController::class, 'index'])->name('me');
        Route::put('', [MeController::class, 'update'])->name('me.update');
    });


    Route::prefix('todos')->middleware('auth:api')->group(function (){
        Route::get('', [TodoController::class, 'index'])->name('list');
        Route::get('{todo}', [TodoController::class, 'show'])->name('get.todo');
        Route::post('', [TodoController::class, 'store'])->name('create');
        Route::put('{todo}', [TodoController::class, 'update'])->name('update');
        Route::delete('{todo}', [TodoController::class, 'destroy'])->name('todos.destroy');
        //Route::post('', [TodoController::class, 'store'])->name('create');
        Route::post('{todo}/tasks', [TodoController::class, 'addTask'])->name('create.task');

    });


    Route::prefix('todo-tasks')->group(function (){
        Route::put('{todoTask}', [TodoTaskController::class, 'update'])->name('todoTask.update');
        Route::delete('{todoTask}', [TodoTaskController::class, 'destroy'])->name('todoTask.destroy');


    });







});
